## OVERALL DESCRIPTION ##
This repository contains test scripts written completely in Python2.7 language:

* using Selenium 2.0 (WebDriver)

* implementing page object pattern

* implementing Python built-in unittest framework


## ASSUMPTIONS ##
Few assumptions have been made to simplify the test scenario:

1. All test scripts are using Chrome WebDriver, hence they automate only Chrome Browser testing.

2. All scripts are using "A Test Project" in public Jira instance - https://jira.atlassian.com/browse/TST

3. All test scripts have one extra, initial step that logs the user to Jira Test Project.

4. New issues are created by defining only "summary" field - all other fields are left with default values.

5. Existing issues are searched/updated only by **unique** "summary" field.


##REQUIREMENTS##
Here is the list of apps/frameworks required as the execution environment:

* OS: preferably Windows (7/8/8.1)

* Google Chrome browser installed

* Python 2.7.x installed

* Selenium module for Python installed (you can use "pip install selenium" if you have pip installed)

* ChromeDriver executable on your local machine (http://chromedriver.storage.googleapis.com/2.12/chromedriver_win32.zip)

* Python executable path added to Windows PATH

* ChromeDriver executable path added to Windows PATH


## INSTRUCTIONS ##
All test cases are located in **jira_test_suite.py** script. To run selected test case execute the following command-line pattern in main repo directory:

*python -m unittest jira_test_suite.<test_case_name>.test_execute*

All initial test case conditions are defined in "setUp()" methods in each test case definition:


```
#!python

self.username = "bartlomiej.kornowski@gmail.com" #example Jira account*
self.password = "testpassword" #example password for Jira account*
self.jira_test_url = "https://jira.atlassian.com/browse/TST" #Jira Test Project URL*
```

# TEST CASE 1 - CreateIssueTest #

This test creates new Issue with summary field set as defined in:

```
#!python

self.issue_summary_to_be_created = "bartkorn bug summary1"
```

**RUN:**

*python -m unittest jira_test_suite.CreateIssueTest.test_execute*

# TEST CASE 2 - SearchIssueTest #

This test uses Jira Search Issue module to find the Issue by unique "summary" field value as defined in:

```
#!python

self.issue_summary_to_be_found = "bartkorn bug summary1"
```

**RUN:**

*python -m unittest jira_test_suite.SearchIssueTest.test_execute*

# TEST CASE 3 - SearchAndUpdateIssueTest #

This test uses Jira Search Issue module to find  and update exisiting Issue by as defined in:

```
#!python

self.issue_summary_to_be_updated = "bartkorn bug summary1"
self.issue_new_summary_value = "bartkorn bug summary2"
```

**RUN:**

*python -m unittest jira_test_suite.SearchAndUpdateIssueTest.test_execute*