import unittest
from selenium import webdriver
from pageobjects.targetpages import HomePage, LoginPage, CreateIssue, SearchPage, UpdateIssue

class SearchIssueTest(unittest.TestCase):

    def setUp(self):
        self.username = "bartlomiej.kornowski@gmail.com"
        self.password = "testpassword"
        self.jira_test_url = "https://jira.atlassian.com/browse/TST"
        self.issue_summary_to_be_found = "bartkorn bug summary1"
        self.driver = webdriver.Chrome()

    def test_execute(self):
        welcome_page = HomePage(self.driver)
        welcome_page.navigate(self.jira_test_url)

        #assert
        login_page = welcome_page.getLoginPage()
        self.assertTrue(login_page, "Cannot get the LogIn page")

        login_page.setUser(self.username)
        login_page.setPass(self.password)

        #assert
        home_page = login_page.login()
        self.assertTrue(home_page, "Cannot log in to Jira account")

        #assert
        search_page = home_page.getSearchPage()
        self.assertTrue(search_page, "Cannot get to Search page")

        result = search_page.search(self.issue_summary_to_be_found)
        self.assertTrue(result, "Issue not found in Jira Test Project")

    def tearDown(self):
        self.driver.close()

class CreateIssueTest(unittest.TestCase):

    def setUp(self):
        self.username = "bartlomiej.kornowski@gmail.com"
        self.password = "testpassword"
        self.jira_test_url = "https://jira.atlassian.com/browse/TST"
        self.issue_summary_to_be_created = "bartkorn bug summary1"
        self.driver = webdriver.Chrome()

    def test_execute(self):
        welcome_page = HomePage(self.driver)
        welcome_page.navigate(self.jira_test_url)


        login_page = welcome_page.getLoginPage()
        self.assertTrue(login_page, "Cannot get the LogIn page")

        login_page.setUser(self.username)
        login_page.setPass(self.password)


        home_page = login_page.login()
        self.assertTrue(home_page, "Cannot log in to Jira account")


        create_page = home_page.getCreateIssuePage()
        self.assertTrue(create_page, "Cannot open Create dialog form")

        create_page.setIssueSummary(self.issue_summary_to_be_created)

        result_page = create_page.createIssue()
        self.assertIsInstance(result_page, HomePage, "Issue could not be created")

    def tearDown(self):
        self.driver.close()

class SearchAndUpdateIssueTest(unittest.TestCase):

    def setUp(self):
        self.username = "bartlomiej.kornowski@gmail.com"
        self.password = "testpassword"
        self.jira_test_url = "https://jira.atlassian.com/browse/TST"
        self.issue_summary_to_be_updated = "bartkorn bug summary1"
        self.issue_new_summary_value = "bartkorn bug summary2"
        self.driver = webdriver.Chrome()

    def test_execute(self):
        welcome_page = HomePage(self.driver)
        welcome_page.navigate(self.jira_test_url)


        login_page = welcome_page.getLoginPage()
        self.assertTrue(login_page, "Cannot get the LogIn page")

        login_page.setUser(self.username)
        login_page.setPass(self.password)


        home_page = login_page.login()
        self.assertTrue(home_page, "Cannot log into Jira account")

        search_page = home_page.getSearchPage()
        self.assertTrue(search_page, "Cannot get to Search page")

        update_page = search_page.update(self.issue_summary_to_be_updated)
        self.assertIsInstance(update_page, UpdateIssue, "Issue could not be updated")

        update_page.updateIssueSummary(self.issue_new_summary_value)
        updated = update_page.updateIssue()
        self.assertTrue(updated, "Issue could not be updated")

    def tearDown(self):
        self.driver.close()
