from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC

class BasePage(object):
    """base class having all low level webdriver functionalilites"""
    url = None

    def __init__(self, driver):
        self.driver = driver

    def load(self, expected_condition):
        loaded = False
        try:
            WebDriverWait(self.driver, 10).until(expected_condition)
            loaded = True
        except:
            return loaded
        return loaded

    def fill_form_by_css(self, css_selector, input_value):
        element = self.driver.find_element(by=By.CSS_SELECTOR, value=css_selector)
        element.send_keys(input_value)

    def fill_form_by_id(self, id, input_value):
        element = self.driver.find_element(by=By.ID, value=id)
        element.send_keys(input_value)

    def click_by_css(self, css_selector):
        element = self.driver.find_element(by=By.CSS_SELECTOR, value=css_selector)
        element.click()

    def click_by_id(self, id):
        element = self.driver.find_element(by=By.ID, value=id)
        element.click()

    def click_by_link_text(self, link):
        element = self.driver.find_element(by=By.LINK_TEXT, value=link)
        element.click()

    def confirm_by_id(self, id):
        element = self.driver.find_element(by=By.ID, value=id)
        element.send_keys(Keys.RETURN)

    def navigate(self, url):
        self.driver.get(url)