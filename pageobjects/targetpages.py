from pageobjects.basepage import BasePage
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

class LoginPage(BasePage):

    def setUser(self, username):
        self.fill_form_by_id("username",username)

    def setPass(self, password):
        self.fill_form_by_id("password", password)

    def login(self):
        self.click_by_id("login-submit")

        if self.load(EC.title_contains("A Test Project - Atlassian JIRA")):
            return HomePage(self.driver)
        else:
            return False

class CreateIssue(BasePage):

    def setIssueSummary(self, summary):
        self.fill_form_by_id("summary", summary)

    def createIssue(self):
        self.click_by_id("create-issue-submit")

        if self.load(EC.presence_of_element_located((By.CSS_SELECTOR, "div[class='aui-message success closeable']"))):
            return HomePage(self.driver)
        else:
            return False

class SearchPage(BasePage):

    def setSearchField(self, query):
        self.fill_form_by_id("searcher-query", query)

    def searchElement(self):
        self.confirm_by_id("searcher-query")

    def foundElement(self, query):

        if not self.load(EC.presence_of_element_located((By.CSS_SELECTOR, "div[class='aui-item list-results-panel']"))):
            return False

        if self.load(EC.text_to_be_present_in_element((By.ID, "summary-val"), query)):
            return True
        else:
            return False

    def search(self, element):
        self.setSearchField(element)
        self.searchElement()
        if self.foundElement(element):
            return True
        else:
            return False

    def update(self, element):

        if not self.search(element):
            return False

        if not self.load(EC.presence_of_element_located((By.ID, "edit-issue"))):
            return False

        if not self.load(EC.visibility_of_element_located((By.CSS_SELECTOR, "#edit-issue > span.icon.jira-icon-edit"))):
            return False

        self.click_by_css("#edit-issue > span.icon.jira-icon-edit")

        if not self.load(EC.presence_of_element_located((By.ID,"edit-issue-dialog"))):
            return False

        if self.load(EC.presence_of_element_located((By.ID,"summary"))):
            return UpdateIssue(self.driver)
        else:
            return False

class UpdateIssue(BasePage):

    def updateIssueSummary(self, summary):
        self.fill_form_by_id("summary", summary)

    def updateIssue(self):
        self.click_by_id("edit-issue-submit")

        if self.load(EC.presence_of_element_located((By.CSS_SELECTOR,"div[class='aui-message success closeable']"))):
            return SearchPage(self.driver)
        else:
            return False

class HomePage(BasePage):

    def getLoginPage(self):
        self.click_by_link_text("Log In")
        if self.load(EC.title_contains("Sign in to continue")):
            return LoginPage(self.driver)
        else:
            return False

    def getCreateIssuePage(self):
        self.click_by_id("create_link")

        if not self.load(EC.presence_of_element_located((By.ID, "create-issue-dialog"))):
            return False

        if self.load(EC.presence_of_element_located((By.ID, "summary"))):
            return CreateIssue(self.driver)
        else:
            return False

    def getSearchPage(self):

        if not self.load(EC.visibility_of_element_located((By.LINK_TEXT, "Issues"))):
            return False

        self.click_by_link_text("Issues")

        if not self.load(EC.visibility_of_element_located((By.ID,"issues_new_search_link_lnk"))):
            return False

        self.click_by_id("issues_new_search_link_lnk")

        if not self.load(EC.presence_of_element_located((By.CSS_SELECTOR,"h1[class='search-title']"))):
            return False

        if self.load(EC.presence_of_element_located((By.CSS_SELECTOR,"div[class='results-panel navigator-item']"))):
            return SearchPage(self.driver)
        else:
            return False
